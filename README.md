Nume: Dociu Andreea Maria
Grupa: 1069
Tema: 19(gestionare resurse de invatare online cu integrare Facebook share)

  Se va realiza o aplicatie cu front-end SPA folosind React.js si back-end 
REST. Prin urmare, serviciul REST va avea o interfata de tip JSON. Operatiile
aplicatiei se vor implementa printr-un serviciu REST. Utilizatorii
si listele de resurse de invatare asociate fiecarui utilizator in parte vor fi
salvate intr-o baza de date, accesul la baza de date facandu-se printr-un API de
persistenta. Se va integra cu Facebook share pe grupuri sau pagini personale. 

  Va exista o pagina de login, astfel incat fiecare utilizator isi va crea 
un cont, pe baza caruia poate folosi aplicatia.

Dupa logare, utilizatorul va dispune de facilitati precum:

  -posibilitatea de a alege materia/materiile pe care isi doreste sa o/le 
studieze

  -posibilitatea de a vizualiza o lista de cursuri de care utilizatorul poate
beneficia/beneficiaza deja
  
  -posibiliatatea de a vizualiza diferite cursuri, in functie de alegerea 
facuta la inceput 

  -optiunea de a da teste sau a face exercitii cu acordarea unui anumit 
punctaj(note) pentru verificarea cunostintelor

  -optiune unde isi poate vedea rezultatele si alte date personale precum 
domeniile de interes
  